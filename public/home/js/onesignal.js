//change sw.js to the name of your previous service worker file
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.getRegistrations().then(function(registrations) {
      for(let registration of registrations) {
        if (registration.active.scriptURL.includes("sw.js")) {
          registration.unregister();
        }
      } 
  });
  }

window.OneSignalDeferred = window.OneSignalDeferred || [];
 OneSignalDeferred.push(async function(OneSignal) {
 await OneSignal.init({
  appId: "90e3c74b-34e4-4fa9-aaa1-60855b8a2c1f",
  });
 });